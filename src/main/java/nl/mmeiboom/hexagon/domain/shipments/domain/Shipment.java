package nl.mmeiboom.hexagon.domain.shipments.domain;

import lombok.Data;

import java.util.List;

@Data
public class Shipment {
    private final List<ShipmentItem> items;
}
