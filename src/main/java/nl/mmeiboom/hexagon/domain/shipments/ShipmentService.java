package nl.mmeiboom.hexagon.domain.shipments;

import nl.mmeiboom.hexagon.domain.shipments.domain.Shipment;

import java.util.Set;

public interface ShipmentService {
    Set<Shipment> getShipmentsContaining(int orderItemId);
}
