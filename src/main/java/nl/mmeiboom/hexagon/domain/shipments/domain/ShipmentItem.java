package nl.mmeiboom.hexagon.domain.shipments.domain;

import lombok.Data;

@Data
public class ShipmentItem {
    private final int id;
    private final int orderItemId;
    private final int quantityShipped;
}
