package nl.mmeiboom.hexagon.domain.returns.impl;

import nl.mmeiboom.hexagon.domain.orders.domain.OrderItem;
import nl.mmeiboom.hexagon.domain.orders.OrderService;
import nl.mmeiboom.hexagon.domain.returns.domain.InsufficientQuantityException;
import nl.mmeiboom.hexagon.domain.returns.domain.RequestToReturnOrderItemsCommand;
import nl.mmeiboom.hexagon.domain.returns.domain.Return;
import nl.mmeiboom.hexagon.domain.returns.domain.ReturnItem;
import nl.mmeiboom.hexagon.domain.returns.ReturnRepository;
import nl.mmeiboom.hexagon.domain.returns.ReturnService;
import nl.mmeiboom.hexagon.domain.returns.domain.UnsupportedProductTypeException;
import nl.mmeiboom.hexagon.domain.shipments.domain.ShipmentItem;
import nl.mmeiboom.hexagon.domain.shipments.ShipmentService;
import org.springframework.stereotype.Service;

import static nl.mmeiboom.hexagon.domain.orders.domain.OrderItem.Type.PHYSICAL;

@Service
public class ReturnServiceImpl implements ReturnService {

    private final OrderService orderService;
    private final ShipmentService shipmentService;
    private final ReturnRepository returnRepository;

    public ReturnServiceImpl(OrderService orderService, ShipmentService shipmentService, ReturnRepository returnRepository) {
        this.orderService = orderService;
        this.shipmentService = shipmentService;
        this.returnRepository = returnRepository;
    }

    @Override
    public void handle(RequestToReturnOrderItemsCommand command) {
        command.getItemsToReturn().forEach(this::validateItemCanBeReturned);
        returnRepository.register(Return.fromCommand(command));
    }

    private void validateItemCanBeReturned(RequestToReturnOrderItemsCommand.ItemToReturn item) {
        validateItemType(item);
        validateQuantity(item);
    }

    private void validateItemType(RequestToReturnOrderItemsCommand.ItemToReturn item) {
        OrderItem orderItem = orderService.getOrderItem(item.getOrderItemId());
        if (orderItem.getType() != PHYSICAL) {
            throw new UnsupportedProductTypeException();
        }
    }

    private void validateQuantity(RequestToReturnOrderItemsCommand.ItemToReturn item) {
        int quantityShipped = getQuantityShipped(item);
        int quantityReturned = getQuantityReturned(item);
        if (quantityShipped - quantityReturned < item.getQuantityToReturn()) {
            throw new InsufficientQuantityException();
        }
    }

    private int getQuantityReturned(RequestToReturnOrderItemsCommand.ItemToReturn item) {
        return returnRepository.getReturnsForItem(item.getOrderItemId()).stream()
                .flatMap(r -> r.getItems().stream())
                .filter(ri -> ri.getOrderItemId() == item.getOrderItemId())
                .mapToInt(ReturnItem::getQuantityReturned)
                .sum();
    }

    private int getQuantityShipped(RequestToReturnOrderItemsCommand.ItemToReturn item) {
        return shipmentService.getShipmentsContaining(item.getOrderItemId()).stream()
                .flatMap(s -> s.getItems().stream())
                .filter(si -> si.getOrderItemId() == item.getOrderItemId())
                .mapToInt(ShipmentItem::getQuantityShipped)
                .sum();
    }

}
