package nl.mmeiboom.hexagon.domain.returns.domain;

import lombok.Data;

import java.util.Set;
import java.util.stream.Collectors;

@Data
public class Return {
    private final Set<ReturnItem> items;

    public static Return fromCommand(RequestToReturnOrderItemsCommand command) {
        // TODO Factory for ID generation? Bit ugly now.
        Set<ReturnItem> returnItems = command.getItemsToReturn().stream()
                .map(requestedItem -> new ReturnItem(0, requestedItem.getOrderItemId(), requestedItem.getQuantityToReturn()))
                .collect(Collectors.toSet());
        return new Return(returnItems);
    }
}
