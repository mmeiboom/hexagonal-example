package nl.mmeiboom.hexagon.domain.returns;

import nl.mmeiboom.hexagon.domain.returns.domain.Return;

import java.util.Set;

public interface ReturnRepository {
    Set<Return> getReturnsForItem(int orderItemId);

    void register(Return aReturn);
}
