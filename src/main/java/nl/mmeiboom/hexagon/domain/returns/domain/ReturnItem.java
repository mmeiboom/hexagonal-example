package nl.mmeiboom.hexagon.domain.returns.domain;

import lombok.Data;

@Data
public class ReturnItem {
    private final int id;
    private final int orderItemId;
    private final int quantityReturned;
}
