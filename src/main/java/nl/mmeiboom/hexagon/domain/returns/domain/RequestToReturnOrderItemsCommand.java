package nl.mmeiboom.hexagon.domain.returns.domain;

import lombok.Data;

import java.util.Set;

@Data
public class RequestToReturnOrderItemsCommand {
    private final Set<ItemToReturn> itemsToReturn;

    @Data
    public static class ItemToReturn {
        private final int orderItemId;
        private final int quantityToReturn;
    }
}
