package nl.mmeiboom.hexagon.domain.returns;

import nl.mmeiboom.hexagon.domain.returns.domain.RequestToReturnOrderItemsCommand;

public interface ReturnService {

    void handle(RequestToReturnOrderItemsCommand command);
}
