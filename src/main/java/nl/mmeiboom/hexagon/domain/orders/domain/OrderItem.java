package nl.mmeiboom.hexagon.domain.orders.domain;

import lombok.Data;

@Data
public class OrderItem {

    public enum Type {
        DIGITAL,
        PHYSICAL,
        SERVICE
    }

    private final int id;
    private final Type type;
}
