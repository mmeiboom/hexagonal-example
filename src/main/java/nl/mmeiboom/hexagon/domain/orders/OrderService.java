package nl.mmeiboom.hexagon.domain.orders;

import nl.mmeiboom.hexagon.domain.orders.domain.OrderItem;

public interface OrderService {
    OrderItem getOrderItem(int orderItemId);
}
