package nl.mmeiboom.hexagon.domain.orders.domain;

import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class Order {
    private final List<OrderItem> items;
    private final LocalDate dateOrdered;
}
