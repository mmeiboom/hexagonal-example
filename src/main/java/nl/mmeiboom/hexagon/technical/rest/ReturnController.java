package nl.mmeiboom.hexagon.technical.rest;

import nl.mmeiboom.hexagon.domain.returns.ReturnService;
import org.springframework.stereotype.Controller;

@Controller
public class ReturnController {

    private final ReturnService returnService;

    public ReturnController(ReturnService returnService) {
        this.returnService = returnService;
    }
}
