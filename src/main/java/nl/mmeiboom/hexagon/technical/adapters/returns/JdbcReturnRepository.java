package nl.mmeiboom.hexagon.technical.adapters.returns;

import nl.mmeiboom.hexagon.domain.returns.ReturnRepository;
import nl.mmeiboom.hexagon.domain.returns.domain.Return;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class JdbcReturnRepository implements ReturnRepository {

    @Override
    public Set<Return> getReturnsForItem(int orderItemId) {
        return null;
    }

    @Override
    public void register(Return aReturn) {

    }
}
