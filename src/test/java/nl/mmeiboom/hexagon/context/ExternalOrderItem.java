package nl.mmeiboom.hexagon.context;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import nl.mmeiboom.hexagon.domain.orders.domain.OrderItem;

import static nl.mmeiboom.hexagon.domain.orders.domain.OrderItem.Type.PHYSICAL;

@Getter
@RequiredArgsConstructor
public class ExternalOrderItem {
    private final int id;
    private int quantityOrdered = 1;
    private int quantityShipped = 0;
    private OrderItem.Type type = PHYSICAL;

    public OrderItem toDomain() {
        return new OrderItem(id, type);
    }

    public ExternalOrderItem ofType(OrderItem.Type type) {
        this.type = type;
        return this;
    }

    public ExternalOrderItem thatHasBeenShippedRecently() {
        this.quantityShipped = this.quantityOrdered;
        return this;
    }
}
