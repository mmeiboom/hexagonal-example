package nl.mmeiboom.hexagon.context;

import java.util.ArrayList;
import java.util.List;

public class ExternalContext {

    public final List<ExternalOrderItem> orderItems = new ArrayList<>();

    public ExternalOrderItem anOrderItem() {
        ExternalOrderItem item = new ExternalOrderItem(1);
        orderItems.add(item);
        return item;
    }

}
