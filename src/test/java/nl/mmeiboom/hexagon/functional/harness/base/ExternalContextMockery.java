package nl.mmeiboom.hexagon.functional.harness.base;

import nl.mmeiboom.hexagon.context.ExternalContext;
import nl.mmeiboom.hexagon.functional.harness.stubs.OrderServiceAnswerSupplier;
import nl.mmeiboom.hexagon.functional.harness.stubs.ShipmentServiceAnswerSupplier;

/**
 * Manages the test boundary for functional tests
 * <p>
 * The test boundary for functional tests is formed by all defined port interfaces, each of which should be backed by a
 * mock or stub during test. These mocks and their behaviour is managed by this mockery class. The state driving mock
 * behaviour is contained in the {@link ExternalContext}. The mocks themselves are prepared via the {@link MockedPortsConfiguration}.
 */
class ExternalContextMockery {

    private static ExternalContextMockery mockery;

    private final ExternalContext externalContext = new ExternalContext();
    private final ServiceLocator serviceLocator;

    private ExternalContextMockery(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    static ExternalContext setupExternalContext(ServiceLocator serviceLocator) {
        if (mockery == null) {
            mockery = new ExternalContextMockery(serviceLocator);
            mockery.bindMocks();
        }
        return mockery.externalContext;
    }

    private void bindMocks() {
        new OrderServiceAnswerSupplier(serviceLocator, externalContext);
        new ShipmentServiceAnswerSupplier(serviceLocator, externalContext);

        // TODO ID generation
    }

//    private Answer<Long> longIdSupplier = invocation -> TestIdGenerator.nextLong();
}
