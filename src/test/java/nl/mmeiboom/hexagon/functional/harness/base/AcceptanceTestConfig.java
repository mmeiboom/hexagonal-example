package nl.mmeiboom.hexagon.functional.harness.base;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"nl.mmeiboom.hexagon.domain", "nl.mmeiboom.hexagon.functional.harness.base"})
public class AcceptanceTestConfig {
}
