package nl.mmeiboom.hexagon.functional.harness.base;

import nl.mmeiboom.hexagon.domain.orders.OrderService;
import nl.mmeiboom.hexagon.domain.returns.ReturnRepository;
import nl.mmeiboom.hexagon.domain.shipments.ShipmentService;
import org.springframework.stereotype.Component;

/**
 * Provides access to ports on the test boundary
 */
@Component
public class ServiceLocator {

    public final OrderService orderService;
    public final ShipmentService shipmentService;
    public final ReturnRepository returnRepository;

    public ServiceLocator(OrderService orderService, ShipmentService shipmentService, ReturnRepository returnRepository) {
        this.orderService = orderService;
        this.shipmentService = shipmentService;
        this.returnRepository = returnRepository;
    }
}
