package nl.mmeiboom.hexagon.functional.harness.stubs;

import nl.mmeiboom.hexagon.domain.shipments.ShipmentService;
import nl.mmeiboom.hexagon.domain.shipments.domain.Shipment;
import nl.mmeiboom.hexagon.domain.shipments.domain.ShipmentItem;
import nl.mmeiboom.hexagon.context.ExternalContext;
import nl.mmeiboom.hexagon.context.ExternalOrderItem;
import nl.mmeiboom.hexagon.functional.harness.base.ServiceLocator;
import org.mockito.stubbing.Answer;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doAnswer;

public class ShipmentServiceAnswerSupplier extends AbstractAnswerSupplier<ShipmentService> {

    public ShipmentServiceAnswerSupplier(ServiceLocator serviceLocator, ExternalContext externalContext) {
        super(serviceLocator.shipmentService, externalContext);
    }

    @Override
    protected void configureAnswers(ShipmentService port, ExternalContext externalContext) {
        doAnswer(usingShipmentSupplier(externalContext)).when(port).getShipmentsContaining(anyInt());
    }

    private Answer<Set<Shipment>> usingShipmentSupplier(ExternalContext externalContext) {
        return invocation -> {
            int orderItemId = (Integer) invocation.getArguments()[0];

            return externalContext.orderItems.stream()
                    .filter(item -> item.getId() == orderItemId)
                    .findAny()
                    .map(this::toShipmentForItem)
                    .orElse(Collections.emptySet());
        };
    }

    private Set<Shipment> toShipmentForItem(ExternalOrderItem externalOrderItem) {
        if(externalOrderItem.getQuantityShipped() > 0) {
            List<ShipmentItem> items = Collections.singletonList(new ShipmentItem(1, externalOrderItem.getId(), externalOrderItem.getQuantityShipped()));
            return Collections.singleton(new Shipment(items));
        }

        return Collections.emptySet();
    }
}
