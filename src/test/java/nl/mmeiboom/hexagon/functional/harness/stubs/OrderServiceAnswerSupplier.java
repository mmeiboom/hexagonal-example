package nl.mmeiboom.hexagon.functional.harness.stubs;

import nl.mmeiboom.hexagon.domain.orders.OrderService;
import nl.mmeiboom.hexagon.domain.orders.domain.OrderItem;
import nl.mmeiboom.hexagon.context.ExternalContext;
import nl.mmeiboom.hexagon.context.ExternalOrderItem;
import nl.mmeiboom.hexagon.functional.harness.base.ServiceLocator;
import org.mockito.stubbing.Answer;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doAnswer;

public class OrderServiceAnswerSupplier extends AbstractAnswerSupplier<OrderService> {

    public OrderServiceAnswerSupplier(ServiceLocator serviceLocator, ExternalContext externalContext) {
        super(serviceLocator.orderService, externalContext);
    }

    @Override
    protected void configureAnswers(OrderService port, ExternalContext externalContext) {
        doAnswer(withItemsSupplier(externalContext)).when(port).getOrderItem(anyInt());
    }

    private Answer<OrderItem> withItemsSupplier(ExternalContext externalContext) {
        return invocation -> {
            int orderItemId = (Integer) invocation.getArguments()[0];

            return externalContext.orderItems.stream()
                    .filter(item -> item.getId() == orderItemId)
                    .findAny()
                    .map(ExternalOrderItem::toDomain)
                    .orElse(null); // Exception?
        };
    }
}
