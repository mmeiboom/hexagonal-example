package nl.mmeiboom.hexagon.functional.harness.base;

import nl.mmeiboom.hexagon.domain.orders.OrderService;
import nl.mmeiboom.hexagon.domain.returns.ReturnRepository;
import nl.mmeiboom.hexagon.domain.shipments.ShipmentService;
import nl.mmeiboom.hexagon.functional.harness.stubs.StubReturnRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static org.mockito.Mockito.mock;


@Configuration
public class MockedPortsConfiguration {

    @Bean
    public OrderService orderService() {
        return mock(OrderService.class);
    }

    @Bean
    public ShipmentService shipmentService() {
        return mock(ShipmentService.class);
    }

    @Bean
    public ReturnRepository returnRepository() {
        return new StubReturnRepository();
    }

}
