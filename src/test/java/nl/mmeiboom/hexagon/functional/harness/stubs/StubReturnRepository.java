package nl.mmeiboom.hexagon.functional.harness.stubs;

import nl.mmeiboom.hexagon.domain.returns.ReturnRepository;
import nl.mmeiboom.hexagon.domain.returns.domain.Return;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class StubReturnRepository implements ReturnRepository {

    private Set<Return> returns = new HashSet<>();

    @Override
    public Set<Return> getReturnsForItem(int orderItemId) {
        return returns.stream()
                .filter(containsItem(orderItemId))
                .collect(Collectors.toSet());
    }

    private Predicate<? super Return> containsItem(int orderItemId) {
        return r -> r.getItems().stream()
                .anyMatch(i -> i.getOrderItemId() == orderItemId);
    }

    @Override
    public void register(Return newReturn) {
        returns.add(newReturn);
    }
}
