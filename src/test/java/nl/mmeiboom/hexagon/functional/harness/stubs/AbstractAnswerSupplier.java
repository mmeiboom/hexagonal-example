package nl.mmeiboom.hexagon.functional.harness.stubs;

import nl.mmeiboom.hexagon.context.ExternalContext;

abstract class AbstractAnswerSupplier<T> {

    AbstractAnswerSupplier(T mock, ExternalContext externalContext) {
        configureAnswers(mock, externalContext);
    }

    protected abstract void configureAnswers(T mock, ExternalContext externalContext);

}
