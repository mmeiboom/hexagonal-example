package nl.mmeiboom.hexagon.functional.harness.base;

import nl.mmeiboom.hexagon.context.ExternalContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static nl.mmeiboom.hexagon.functional.harness.base.ExternalContextMockery.setupExternalContext;


/**
 * Provide context initialization for acceptance tests.
 * - exercises the functional core
 * - using the core's public ports/adapters to communicate with it
 * - using an external context to orchestrate mocks and stubs
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {AcceptanceTestConfig.class})
public abstract class AcceptanceTestBase {

    @Autowired
    private ServiceLocator serviceLocator;

    protected ExternalContext given;

    @BeforeEach
    public void setup() {
        given = setupExternalContext(serviceLocator);
    }
}