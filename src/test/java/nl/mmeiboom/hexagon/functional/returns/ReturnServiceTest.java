package nl.mmeiboom.hexagon.functional.returns;

import nl.mmeiboom.hexagon.domain.returns.ReturnService;
import nl.mmeiboom.hexagon.domain.returns.domain.RequestToReturnOrderItemsCommand;
import nl.mmeiboom.hexagon.context.ExternalOrderItem;
import nl.mmeiboom.hexagon.functional.harness.base.AcceptanceTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;
import java.util.Set;

class ReturnServiceTest extends AcceptanceTestBase {

    @Autowired
    public ReturnService returnService;

    @Test
    void registerReturnForReturnableItem() {
        ExternalOrderItem externalOrderItem = given
                .anOrderItem()
                .thatHasBeenShippedRecently();

        returnService.handle(requestToReturn(externalOrderItem));
    }

    private RequestToReturnOrderItemsCommand requestToReturn(ExternalOrderItem externalOrderItem) {
        Set<RequestToReturnOrderItemsCommand.ItemToReturn> items = Collections.singleton(new RequestToReturnOrderItemsCommand.ItemToReturn(externalOrderItem.getId(), 1));
        return new RequestToReturnOrderItemsCommand(items);
    }
}
