## The Application

This demo application serves to demonstrate several concepts around design, testing and integration
an application using hexagonal architecture.

#### References:

* Spaces Summit 2017 - [Building a boomerang that doesn't come back](https://techlab.bol.com/video-insights-building-boomerang-doesnt-come-back-haunt-spaces-summit/)
* Beyondxscratch - [Decoupling your technical code from your business logic](https://beyondxscratch.com/2017/08/19/decoupling-your-technical-code-from-your-business-logic-with-the-hexagonal-architecture-hexarch/)
* Duncan Nisbet - [Hexagonal Architecture for testers](http://www.duncannisbet.co.uk/hexagonal-architecture-for-testers-part-1)
* Nat Pryce - [Visualising Test Terminology](http://www.natpryce.com/articles/000772.html)

#### API References:

* Mockito Answer Supplier - [https://static.javadoc.io/org.mockito/...#Java_8_Custom_Answers](https://static.javadoc.io/org.mockito/mockito-core/2.27.0/org/mockito/Mockito.html#Java_8_Custom_Answers)
* WireMock Response Transformers - [http://wiremock.org/docs/extending-wiremock/](http://wiremock.org/docs/extending-wiremock/)